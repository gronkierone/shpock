package com.shpock.shpockassignment

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.shpock.shpockassignment.api.DataState
import com.shpock.shpockassignment.utils.safeApiCall
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.*
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException

class NetworkHelperTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun testSuccessApiCall() {
            val lambdaResult = true
            val result = runBlocking {
             safeApiCall { lambdaResult }
           }
            assertEquals(DataState.Success(lambdaResult), result)
    }

    @Test
    fun testFailedApiCall() {
        val result = runBlocking {
            safeApiCall { throw IOException() }
        }
        assertEquals(DataState.Error("NETWORK"), result)
    }
}