package com.shpock.shpockassignment.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import com.shpock.shpockassignment.models.ShipsModelResponse
import com.shpock.shpockassignment.repository.AppRepository
import com.shpock.shpockassignment.api.DataState
import junit.framework.TestCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doReturn
import org.mockito.kotlin.mock

@ExperimentalCoroutinesApi
class ShipsViewModelTest : TestCase() {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    private val liveData = mock<LiveData<DataState<ShipsModelResponse>>> {  }

    private val repository = mock<AppRepository>() {
        on { getShips() } doReturn liveData
    }

    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    @Before
    override fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
    }

    @After
    override fun tearDown() {
        Dispatchers.resetMain() // reset main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun testGetShipsResponse() {
        val viewModel = ShipsViewModel(repository)
        assertEquals(liveData, viewModel.shipsResponse)
    }
}