package com.shpock.shpockassignment

import com.shpock.shpockassignment.utils.getGreetings
import junit.framework.TestCase
import org.junit.Test

class StringExtTest {

    @Test
    fun checkGreetingType() {
        TestCase.assertEquals( "Arrr!", "ar".getGreetings())
        TestCase.assertEquals( "Aye Aye!", "ay".getGreetings())
        TestCase.assertEquals( "Ahoi!", "ah".getGreetings())
        TestCase.assertEquals( "Ahoi!", null.getGreetings())
        TestCase.assertEquals( "Ahoi!", "asdadasd".getGreetings())
        TestCase.assertEquals( "Ahoi!", "".getGreetings())
        TestCase.assertEquals( "Yo ho hooo!", "yo".getGreetings())
    }
}