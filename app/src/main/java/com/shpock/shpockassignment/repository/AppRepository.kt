package com.shpock.shpockassignment.repository

import androidx.lifecycle.liveData
import com.shpock.shpockassignment.api.ShipsService
import com.shpock.shpockassignment.api.DataState
import com.shpock.shpockassignment.utils.safeApiCall
import kotlinx.coroutines.Dispatchers

class AppRepository (
    private val shipsService: ShipsService
    ) {

     fun getShips() = liveData(Dispatchers.IO) {
        emit(DataState.Loading)
        emit(safeApiCall { shipsService.getShips() })
    }
}