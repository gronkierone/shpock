package com.shpock.shpockassignment.models

enum class GreetingType(val type: String) {
    AHOY("ah"),
    AYE("ay"),
    ARR("ar"),
    YOHOO("yo")
}