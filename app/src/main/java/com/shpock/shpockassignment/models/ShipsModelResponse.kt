package com.shpock.shpockassignment.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class ShipsModelResponse (
    @SerializedName("success") val isSuccess : Boolean,
    @SerializedName("ships") val ships : List<Ship>?
) : Parcelable

@Parcelize
data class Ship(
    @SerializedName("id") val id : Int?,
    @SerializedName("title") val title : String? = "Defult title",
    @SerializedName("description") val description : String? = "",
    @SerializedName("price") val price : Int? = 0,
    @SerializedName("image") val image : String? = "",
    @SerializedName("greeting_type") val greetingType : String? = "",
    ) : Parcelable