package com.shpock.shpockassignment.utils

import com.shpock.shpockassignment.api.DataState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

fun getErrorMessage(responseBody: ResponseBody?): String = try {
    val jsonObject = JSONObject(responseBody!!.string())
    when {
        jsonObject.has(MESSAGE) -> jsonObject.getString(MESSAGE)
        jsonObject.has(ERROR) -> jsonObject.getString(ERROR)
        else -> GENERIC_ERROR
    }
} catch (e: Exception) { GENERIC_ERROR }


suspend inline fun <T : Any> safeApiCall(
    crossinline responseFunction: suspend () -> T
): DataState<T> = try {
    val response = withContext(Dispatchers.IO) { responseFunction.invoke() }
    DataState.Success(response)
} catch (e: Exception) {
    val error = withContext(Dispatchers.Main) {
        e.printStackTrace()
        when (e) {
            is HttpException -> {
                val body = e.response()?.errorBody()
                DataState.Error(getErrorMessage(body))
            }
            is SocketTimeoutException -> DataState.Error(TIMEOUT)
            is IOException -> DataState.Error(NETWORK)
            else -> DataState.Error(GENERIC_ERROR)
        }
    }
    error
}