package com.shpock.shpockassignment.utils

const val GENERIC_ERROR = "Something went wrong"
const val TIMEOUT = "TIMEOUT"
const val NETWORK = "NETWORK"
const val MESSAGE = "message"
const val ERROR = "error"
const val DEFAULT_CHANNEL = "default_channel"
const val SHIPS = "Ships"