package com.shpock.shpockassignment.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import androidx.annotation.DrawableRes
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.shpock.shpockassignment.R
import com.shpock.shpockassignment.views.MainActivity

fun Context.createNotificationChannel(
    channelId: String,
    channelName: String
) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelId, channelName, importance)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
}

fun Context.sendNotification(
    channelId: String,
    id: Int,
    title: String?,
    text: String?,
    requestCode: Int = 0,
    @DrawableRes iconRes: Int = R.drawable.app_icon,
    largeIcon: Bitmap? = null,
    priority: Int = NotificationCompat.PRIORITY_DEFAULT,
    autoCancel: Boolean = true,
) {
    val intent = Intent(this, MainActivity::class.java)
    val pendingIntent: PendingIntent = PendingIntent.getActivity(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT)
    val notificationBuilder = NotificationCompat.Builder(this, channelId).apply {
        setSmallIcon(iconRes)
        setContentTitle(title)
        if (text != null) setContentText(text)
        this.priority = priority
        setContentIntent(pendingIntent)
        setChannelId(channelId)
        setAutoCancel(autoCancel)
        if (largeIcon != null) setLargeIcon(largeIcon)
    }.build()
    with(NotificationManagerCompat.from(this)) {
        notify(id, notificationBuilder)
    }
}