package com.shpock.shpockassignment.utils

import com.shpock.shpockassignment.models.GreetingType

fun String?.getGreetings() = when(this){
    GreetingType.AHOY.type -> "Ahoi!"
    GreetingType.AYE.type -> "Aye Aye!"
    GreetingType.ARR.type -> "Arrr!"
    GreetingType.YOHOO.type -> "Yo ho hooo!"
    else -> "Ahoi!"
}