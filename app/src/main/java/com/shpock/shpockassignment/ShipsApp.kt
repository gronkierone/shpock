package com.shpock.shpockassignment

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ShipsApp : Application() {
}