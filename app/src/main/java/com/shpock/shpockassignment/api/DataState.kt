package com.shpock.shpockassignment.api

import com.shpock.shpockassignment.utils.GENERIC_ERROR

sealed class DataState <out T: Any> {
    object Loading : DataState<Nothing>()
    data class Success <out T: Any>(val data: T) : DataState<T>()
    data class Error(val message: String = GENERIC_ERROR) : DataState<Nothing>()
}