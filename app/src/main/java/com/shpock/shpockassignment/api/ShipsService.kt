package com.shpock.shpockassignment.api

import com.shpock.shpockassignment.models.ShipsModelResponse
import retrofit2.http.GET

interface ShipsService {

    @GET("pirateships")
    suspend fun getShips() : ShipsModelResponse
}