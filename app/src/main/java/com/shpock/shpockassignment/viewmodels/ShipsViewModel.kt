package com.shpock.shpockassignment.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.shpock.shpockassignment.models.ShipsModelResponse
import com.shpock.shpockassignment.repository.AppRepository
import com.shpock.shpockassignment.api.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ShipsViewModel @Inject constructor(
    appRepository: AppRepository
) : ViewModel() {

    val shipsResponse : LiveData<DataState<ShipsModelResponse>> = appRepository.getShips()
}