package com.shpock.shpockassignment.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.shpock.shpockassignment.R
import com.shpock.shpockassignment.databinding.ItemShipBinding
import com.shpock.shpockassignment.models.Ship
import com.shpock.shpockassignment.utils.loadImageWithCrossfade


class ShipAdapter(
    private val selectionListener: (Ship) -> Unit
) : ListAdapter<Ship, RecyclerView.ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ShipViewHolder(
        ItemShipBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        (holder as ShipViewHolder).bind(getItem(position), selectionListener)

    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Ship>() {
            override fun areItemsTheSame(
                oldItem: Ship,
                newItem: Ship
            ) = oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: Ship,
                newItem: Ship
            ) = oldItem == newItem
        }
    }

    class ShipViewHolder(private val binding: ItemShipBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(
            ship: Ship,
            listener: (Ship) -> Unit
        ) {
            binding.apply {
                tvShipName.text = ship.title ?: binding.root.context.getString(R.string.default_name)
                tvPrice.text = "${ship.price}$"
                ivImage.loadImageWithCrossfade(ship.image)
                root.setOnClickListener { listener.invoke(ship) }
            }
        }
    }
}