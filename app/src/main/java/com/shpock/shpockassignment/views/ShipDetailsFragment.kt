package com.shpock.shpockassignment.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.shpock.shpockassignment.databinding.FragmentShipDetailsBinding
import com.shpock.shpockassignment.databinding.FragmentShipListBinding
import com.shpock.shpockassignment.utils.*

class ShipDetailsFragment : Fragment() {

    private var _binding: FragmentShipDetailsBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<ShipDetailsFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShipDetailsBinding.inflate(inflater, container, false)
        binding.apply {
            args.ship.let {
                tvHeader.text = it.title
                tvDescription.text = it.description
                ivBackground.loadImageWithCrossfade(it.image)
                tvPrice.text = "${it.price}$"
                tvGreetings.setOnClickListener { view->
                        this@ShipDetailsFragment.requireContext().run {
                            createNotificationChannel(DEFAULT_CHANNEL, SHIPS)
                            sendNotification(DEFAULT_CHANNEL, it.id ?: 0, SHIPS, it.greetingType.getGreetings())
                        }
                    }
            }
        }
        return binding.root
    }
}