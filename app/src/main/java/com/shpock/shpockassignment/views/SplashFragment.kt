package com.shpock.shpockassignment.views

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.shpock.shpockassignment.R
import com.shpock.shpockassignment.api.DataState
import com.shpock.shpockassignment.utils.GENERIC_ERROR
import com.shpock.shpockassignment.viewmodels.ShipsViewModel

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
class SplashFragment : Fragment(R.layout.fragment_splash) {

    private val viewModel by viewModels<ShipsViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch(Dispatchers.Main) {
            delay(3000)
            viewModel.shipsResponse.observe(viewLifecycleOwner) {
                when (it) {
                    is DataState.Error -> Toast.makeText(
                        requireContext(),
                        it.message,
                        Toast.LENGTH_LONG
                    ).show()
                    is DataState.Success -> {
                        if (it.data.isSuccess) it.data.ships?.let {
                            findNavController().navigate(SplashFragmentDirections.actionSplashFragmentToShipListFragment(ships = it.filterNotNull().toTypedArray()))
                        }
                        else Toast.makeText(requireContext(), GENERIC_ERROR, Toast.LENGTH_LONG).show()
                    }
                    is DataState.Loading -> Unit
                }
            }
        }
    }
}