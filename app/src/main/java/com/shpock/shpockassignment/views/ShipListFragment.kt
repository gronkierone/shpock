package com.shpock.shpockassignment.views

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.appbar.AppBarLayout
import com.shpock.shpockassignment.R
import com.shpock.shpockassignment.databinding.FragmentShipListBinding
import com.shpock.shpockassignment.utils.gone
import com.shpock.shpockassignment.utils.visible
import com.shpock.shpockassignment.views.adapters.ShipAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ShipListFragment : Fragment() {

    private var _binding: FragmentShipListBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<ShipListFragmentArgs>()
    private val shipAdapter by lazy { ShipAdapter {
        findNavController().navigate(ShipListFragmentDirections.actionShipListFragmentToShipDetailsFragment(it))
    } }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentShipListBinding.inflate(inflater, container, false)
        binding.apply {
            rvShips.adapter = shipAdapter.apply {
                submitList(args.ships?.toMutableList())
            }
            appbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
                when {
                    kotlin.math.abs(verticalOffset) - appBarLayout.totalScrollRange == 0 -> {
                        toolbarCollapsedHeader.text = getString(R.string.ship_list_header)
                        tvHeader.gone()
                    }
                    verticalOffset == 0 -> {
                        toolbarCollapsedHeader.text = ""
                    }
                    else -> {
                        toolbarCollapsedHeader.text = ""
                        tvHeader.visible()
                    }
                }
            })
        }
        return binding.root
    }
}